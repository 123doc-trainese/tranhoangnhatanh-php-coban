<?php
$b = 1234;
echo $b;
echo " \n";


define('cost', ['1', '2', '3', '4', '5', '6', '67']); //
const a = [1, 2, 3, 4, 5, 6];
echo cost[1], PHP_EOL . "\n";
echo " \n";
echo a[1], PHP_EOL;

$array = [1, 2, 3, 4, 5, 6, 6, 7, 8];
echo $array[1];
echo " \n";

$a = 5;
$b = 3;
echo "$a nhân $b = " . $a * $b;
echo " \n";
echo "$a chia $b = " . $a / $b;
echo " \n";

if ($a % $b == 0) {
    echo "$a chia hết cho $b \n";
    echo "<br>";
} else {
    echo "$a không chia hết cho $b \n";
}

switch ($array[4]) {
    case "1":
        # code...
        echo "phần tử thứ 1 của mảng là 1 \n";
        break;

    default:
        # code...
        echo "phần tử thứ 5 của mảng là $array[4] \n";
        break;
}

for ($i = 0; $i < count($array); $i++) {
    echo $array[$i];
    echo ",";
}
echo "\n";

const VERS = 1; //  tạo tại thời gian biên dịch
define('VERSION', 1); // tạo tại thời gian chạy 

for ($i = 0; $i < 6; $i) {
    define('VERSION_' . $i, 1 << $i);
}

if (1 < 2) {
    define('VERSION_1', 1); // valid
    // const VERSION = '1.0'; invalid
}