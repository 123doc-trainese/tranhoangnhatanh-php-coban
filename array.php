<?php
$a = ['NHAT ANH' => '2', 'nhat anh' => '4', 'aa' => '6', 'bbbb' => '8', 'CCCC' => '9'];
$key = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$value = ['a', 'a', 'a', 'a', 'e', 'f', 'g', 'h', 'a', 'j'];

"tran haong nhat anh";
array_change_key_case($a, CASE_UPPER); // viết hoa các key trong mảng
// print_r(array_change_key_case($a, CASE_UPPER));
array_change_key_case($a, CASE_LOWER); // viết  thường các phần tử  trong mảng

array_chunk($a, 3); // chia mảng thành mảng 2 chiều
print_r(array_chunk($a, 2)); //

array_combine($key, $value); // gộp 2 mảng thành key => value (cùng độ dài)
// print_r(array_combine($key, $value));

array_count_values($value); // đếm phần tử 'a' có trong mảng

print_r(array_merge($key, $value)); // gộp 2 mảng làm 1

array_key_exists('aaaaaa', $a); // kiểm tra key có tồn tại trong mảng không

// echo array_pop($key); //xóa phần tử cuối cùng
array_push($key, '11'); //chèn  các phần tử vào mảng

print_r($key); //



$a = array_map(function ($item) {
    return $item * 2;
}, $key);

print_r($a);