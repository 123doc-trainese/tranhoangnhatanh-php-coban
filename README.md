# TranHoangNhatAnh-php-coban

## Lý thuyết
    - cài php 8.1
    - chạy code php qua terminal, build-in-server
# Yêu cầu
- Các kiến thức tối thiểu phải lắm được: 
 - PHP cơ bản      
  - Introduction to web Technology     
  - Introduction to HTML5 and CSS     
  - PHP introduction     
  - PHP syntax     
  - PHP Data Types     
  - PHP Variables ,Constants and Array     
  - PHP Operators and Control Statements         
    - if else statement         
    - else if statement         
    - switch case         
    - Loops            
    - while            
    - do while            
    - for            
    - foreach      
  - String Functions      
  - Array Functions 
## Nắm được
- php cơ bản
- tạo file php : sử dụng `<?php` để khai báo php
  ```
    <?php echo "Hello World!"; ?>
  ```
- khai báo biến, hằng, mảng
  ```
    // khai báo biến
    $a = 1;
    
    $b = 'tran hoang nhat anh';

    // khai báo hằng
    const Const = 'nhat anh';

    define('Const','nhat anh');

    // khai báo mảng
    $a = array();
    // hoặc 
    $a = [];
  ```
- if else, vòng lặp for, while, foreach, switch case
- if else:
    ```
    $t = date("H");

    if ($t < "20") {
      echo "Have a good day!";
    } else {
      echo "Have a good night!";
    }

    // kết quả
    Have a good day!
    ```
- for và foreach:
    ```
    for ($x = 0; $x <= 3; $x++) {
      echo "The number is: $x \n";
    }
    // kết quả
    The number is: 0
    The number is: 1
    The number is: 2
    The number is: 3

    $colors = array("red", "green", "blue", "yellow");

    foreach ($colors as $value) {
      echo "$value\n";
    }

    // kết quả
    red
    green
    blue
    yellow
    ```
- while và do while:
    ```
    $x = 1;

    while($x <= 5) {
      echo "The number is: $x \n";
      $x++;
    }
    // kết quả
    The number is: 1 
    The number is: 2 
    The number is: 3 
    The number is: 4 
    The number is: 5 

    // do while
    $x = 6;

    do {
      echo "The number is: $x \n";
      $x++;
    } while ($x <= 5);
    // kết quả
    The number is: 6 
    ```
- các hàm sử lý mảng:
- array_map(): hàm này lặp các phần tử trong mảng và truyền vào hàm tự tạo,
    ```
    $array = [1,2,3,4,5];
    $result = array_map(function($items) {
      return $items * 2;
    }, $array);
    var_dump($result);

    // Kết quả
    [2,4,6,8,10]
    ```
- array(): hàm dùng để tạo mảng
  ```
  $a = array(1,2,3,4,5);

  // tương tự như array()
  $b = [1,2,3,4];

  var_dump($a);
  var_dump($b);
  // kết quả
  $a = array(1,2,3,4,5);
  array(5) {[0]=>int(1),[1]=>int(2),[2]=>int(3),[3]=>int(4),[4]=>int(5)}

  $b = [1,2,3,4];
  array(4) {[0]=>int(1),[1]=>int(2),[2]=>int(3),[3]=>int(4)}
  ```
- array_change_key_case(): hàm chuyển `key` của mảng về dạng viết hoa hoặc viết thường
  ```
  $value = ['a'=>1, 'b'=>2, 'c'=>3, 'd'=>4];
  print_r(array_change_key_case($value,CASE_UPPER));
  // kết quả
  Array([A] => 1,[B] => 2,[C] => 3,[D] => 4);
  ```
- array_chunk() : hàm này có tác dụng cắt 1 mảng thành mảng mới, trong đó mỗi mảng mới có n phần tử
  ```
  $key = [1,2,3,4,5];
  print_r(array_chunk($key,3));
  // kết quả
  Array
  (
      [0] => Array
          (
              [0] => 1
              [1] => 2
              [2] => 3
          )

      [1] => Array
          (
              [0] => 4
              [1] => 5
          )

  )
  ```
- array_combine(): hàm này trộn 2 mảng với nhau với mảng 1 là `key` mảng 2 là `value`. nếu 2 mảng có độ dài không bằng nhau => false
  ```
  $key = [1, 2, 3, 4, 5];
  $value = ['a', 'b', 'c', 'd', 'e'];
  $result = array_combine($key, $value);
  print_r($result);
  // kêt quả
  Array
  (
      [1] => a
      [2] => b
      [3] => c
      [4] => d
      [5] => e
  )
  ```
  ```
    $key = [1, 2, 3];
    $value = ['a', 'b', 'c', 'd', 'e'];
    $result = array_combine($key, $value);
    print_r($result);
    // kết quả
    PHP Fatal error
  ```
- array_count_values(): hàm này dùng để đếm số lần xuất hiện của value trong mảng. trả về 1 mảng có `key` chính là giá trị các phần tử và `value` là số lần xuất hiện
  ```
  $value = [1, 2, 3, 4, 4, 4, 4, 5, 3, 1, 2, 0];
  print_r(array_count_values($value));
  // kết quả
  Array
  (
      [1] => 2 
      [2] => 2
      [3] => 2
      [4] => 4
      [5] => 1
      [0] => 1
  )
  ```
- array_diff(): hàm này dùng để so sánh sự khác nhau giữa 2 hay nhiều mảng và trả về phần tử có `key` và `value` mà `value` chỉ xuất hiện ở mảng đầu
  ```
  $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
  $a2=array("e"=>"red","f"=>"green","g"=>"blue");

  $result=array_diff($a1,$a2);
  print_r($result);
  // kết quả
  Array
  (
      [d] => yellow
  )
  ```
- array_diff_assoc(): hàm này dùng để so sánh mảng 1 và mảng 2 và trả về phần tử có `key` và `value` mà `key` hoặc `value` chỉ xuất hiện mảng 1
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $a2=array("a"=>"red","b"=>"green","c"=>"blue");

    $result=array_diff_assoc($a1,$a2);
    print_r($result);
    // kết quả
    Array
    (
        [d] => yellow
    )
  ```
  ```
  $a1 = array("a" => "red", "b" => "green", "c" => "black", "d" => "yellow");
  $a2 = array("a" => "red", "b" => "green", "c" => "blue");

  $result = array_diff_assoc($a1, $a2);
  print_r($result);
  // kết quả
  Array
  (
      [c] => black
      [d] => yellow
  )
  ```
- array_diff_key(): hàm này dùng để so sánh 2 mảng và trả về phần tử có `key` và `value` mà `key` chỉ có ở mảng 1
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"black");
    $a2=array("a"=>"red","c"=>"blue","d"=>"pink");

    $result=array_diff_key($a1,$a2);
    print_r($result);
    // kết quả
    Array
    (
        [b] => green
    )
  ```   
- array_fill(): hàm này dùng để tạo 1 mảng mới có số phần tử tương ứng khi truyền vào và gán  toàn bộ giá trị là `value` được truyền vào
  ```
  $a1=array_fill(3,4,"blue");
  $b1=array_fill(0,1,"red");

  print_r($a1);
  print_r($b1);
  // kết quả
  // $a1
  Array
  (
      [3] => blue
      [4] => blue
      [5] => blue
      [6] => blue
  )
  // $b1
  Array
  (
      [0] => red
  )
  ```
- array_fill_keys():   hàm này dùng để điền `value` truyền vào vào mảng với `key` truyền từ mảng vào
  ```
  $keys=array("a","b","c","d");
  $a1=array_fill_keys($keys,"blue");
  print_r($a1);
  // kết quả
  Array
  (
      [a] => blue
      [b] => blue
      [c] => blue
      [d] => blue
  )
  ```
- array_filter(array, callbackfunction): hàm này dùng để duyệt mỗi phần tử của mảng truyền vào hàm callback, nếu callback trả về true thì `value` được trả về. (giữ nguyên `key`)
  ```
    function test_odd($var)
    {
    return($var & 1);
    }

  $a1=array(1,3,2,3,4);
  print_r(array_filter($a1,"test_odd"));

  // kết quả
  Array
  (
      [0] => 1
      [1] => 3
      [3] => 3
  )
  ```
- array_key_exists(): hàm này dùng để kiểm tra `key` có trong mảng hay không
  ```
  $a = [1=>0, 2=>1, 3=>2, 4=>3, 5=>4, 6=>4, 7=>4];

  if (array_key_exists(2, $a)) 
  {
    echo  "Key exists";
  } else {
    echo "Key do not exist";
  }
  // kết quả
  Key exists;
  ```  
- array_keys(): hàm này dùng để trả về tập hợp các `key` của mảng
  ```
  $a = [1=>0, 2=>1, 3=>2, 4=>3, 5=>4, 6=>4, 7=>4];
  print_r(array_keys($a));

  // kết quả
  Array
  (
      [0] => 1
      [1] => 2
      [2] => 3
      [3] => 4
      [4] => 5
      [5] => 6
      [6] => 7
  )
  ```
- array_merge(): hàm này dùng để gộp 2 mảng làm 1 ( tạo thành 1 mảng mới)
  ```
    $a = [1, 2, 3];
    $b = [0, 9, 1, 2];
    $result = array_merge($a, $b);
    print_r($result);
    // kết quả
    Array
    (
        [0] => 1
        [1] => 2
        [2] => 3
        [3] => 0
        [4] => 9
        [5] => 1
        [6] => 2
    )
  ```
- [] + []: gộp 2 mảng làm 1. nếu có `value` giống nhau thì không thêm 
  ```
    $a = [1, 2, 3] + [3, 2, 1];
    print_r($a);

    // kết quả
    Array
    (
        [0] => 1
        [1] => 2
        [2] => 3
    )
  ```
- array_slice(): hàm này dùng để lấy ra các phần tử đã chọn trong mảng
  ```
    $a=array("red","green","blue","yellow","brown");
    print_r(array_slice($a,2));

    // kết quả
    Array
    (
      [0] => blue
      [1] => yellow
      [2] => brown
    )
  ```
- array_pop(): hàm này dùng để trả về phần tử cuối cùng của mảng và loại bỏ phần tử đó ở mảng
  ```
    $array = [1, 2, 3];
    print_r(array_pop($array));
    print_r($array);

    //kết quả
    
    //print_r(array_pop($array));
    3

    //print_r($array);
    Array
    (
        [0] => 1
        [1] => 2
    )
  ```
- array_shift(): hàm này dùng để lấy ra phần tử đầu tiên của mảng và loại bỏ phần tử đó khỏi mảng
  ```
    $array = [1, 2, 3];
    print_r(array_shift($array));
    print_r($array);

    // kêt quả
    1
    Array
    (
      [0] => 2
      [1] => 3
    )
  ```
- array_pad(): hàm này dùng để thêm 1 hoặc nhiều phần tử cùng giá trị cho đến khi độ dài đạt đến độ dài định sẵn
  ```
    $array = [1, 2, 3, 4];
    print_r(array_pad($array,6,'anh'));
    // kết quả
    Array
    (
        [0] => 1
        [1] => 2
        [2] => 3
        [3] => 4
        [4] => anh
        [5] => anh
    )
  ```   
- array_push(): hàm này dùng để thêm 1 hoặc nhiều phần tử vào cuối mảng(không tạo mảng mới)
  ```
  $a = [1, 2, 3];

  array_push($a, 1,3,4);
  
  print_r($a);
  // kết quả
  Array
  (
      [0] => 1
      [1] => 2
      [2] => 3
      [3] => 1
      [4] => 3
      [5] => 4
  )
  ``` 
- các hàm sử lý chuỗi:
- explode(): hàm này dùng để chuyển 1 chuỗi thành mảng dựa vào ký tự phần cách
    ```
      print_r(explode(' ', "tran hoang nhat anh"));
      // kết quả
      Array
      (
          [0] => tran
          [1] => hoang
          [2] => nhat
          [3] => anh
      )
    ```  
- implode(): hàm này dùng để nối các phần tử trong mảng thành chuỗi và thêm phần tử phân cách( ngược lại explode)
  ```
    print_r(implode(' ',['tran','hoang','nhat','anh']));
    // kết quả
    tran hoang nhat anh
  ```
- strlen(): hàm này dùng để đếm số ký tự của chuỗi (đếm số byte chuỗi sử dụng)
  ```
  print_r(strlen('tran hoang nhat anh'));

  // kết quả
  19
  ```
- mb_strlen: hàm này dùng để đếm số ký tự của chuỗi (đếm số ký tự kể cả ký tự 2 byte)
  ```
    print_r(strlen('nhật anh'));
    // kết quả
    10


    print_r(mb_strlen('nhật anh'));
    // kết quả
    8
  ```
- str_word_count(): hàm này dùng để đếm từ trong chuỗi
  ```
    $a = 'tran hoang nhat anh';
    print_r(str_word_count($a));

    // kết quả
    4
  ```
- str_repeat(): hàm này dùng để lặp chuỗi n lần
  ```
  print_r(str_repeat('anh', 3));
  // kết quả
  anhanhanh
  ```
- str_replace($find, $replace, $string): hàm này dùng để thay thế `find` bằng `replace` trong chuỗi `string`
  ```
  print_r(str_replace('anh', 'Anh','Tran Hoang Nhat anh'));
  // kêt quả
  Tran Hoang Nhat Anh
  ```
- strstr(): tìm kiếm vị trí đầu tiên xuất hiện của một kí tự hoặc một chuỗi nào đó trong chuỗi nguồn. Hàm trả về một phần của chuỗi gốc tính từ vị trí xuất hiện đầu tiên của kí tự đến vị trí cuối cùng của chuỗi gốc.
  ```
    print_r(strstr('tran hoang nhat anh','nhat'));
    // kết quả
    nhat anh
  ```
- strpos(): hàm này dùng để tìm vị trí đầu tiên của ký tự hoặc chuỗi con trong chuỗi nguồn
  ```
    print_r(strpos('tran hoang nhat anh', 'hoang'));
    // kết quả
    5
  ```
- strripos(): hàm này dùng để tìm vị trí cuối cùng của ký tự hoặc chuỗi con trong chuỗi nguồn
  ```
   print_r(strripos('tran hoang nhat hoang anh','hoang'));
   // kết quả
   16
  ```
- ucwords(): hàm này dùng để viết hoa chữ cái đầu tiên của từ
  ```
    print_r(ucwords('tran hoang nhat anh'));
    // kết quả
    Tran Hoang Nhat Anh
  ```
- trim(): hàm này dùng để loại bỏ khoảng trắng hoặc ký tự 2 đầu của chuỗi
  ```
    print_r(trim('     tran hoang nhat anh        '));

    print_r(trim('hhhhhhtran hoang nhat anhhhh','h'));
    // kết quả
    // print_r(trim('     tran hoang nhat anh        '));
    tran hoang nhat anh

    //print_r(trim('hhhhhhtran hoang nhat anhhhh','h'));
    tran hoang nhat an
  ```
- ltrim(): hàm này dùng để loại bỏ khoảng trắng hoặc ký tự bên trái
 ```
    $str = "Hello World!";
    echo ltrim($str,"Hello");

    // kết quả
     World!
 ```
- rtrim(): hàm này dùng để loại bỏ khoảng trắng hoặc ký tự bên phải
  ```
    $str = "Hello World!";
    echo rtrim($str,"World!");

    // kết quả
    Hello 
  ```
